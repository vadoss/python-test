#!/usr/bin/env python

import os
import shutil

destination = '/usr/local/lib'
soft_link_destination = '/usr/local/bin'

folder_name = 'test_program'
soft_link_name = 'py_test_link'
main_file_name = 'test.py'


def setup():
	full_path = os.path.join(destination, folder_name)
	link_path = os.path.join(soft_link_destination, soft_link_name)	

	if os.path.exists(full_path):
		shutil.rmtree(full_path)

	if os.path.lexists(link_path):
		os.remove(link_path)

	os.mkdir(full_path)

	for root, dirs, files in os.walk(folder_name):
		for file_ in files:
			shutil.copy(os.path.join(root, file_), full_path)

	os.symlink(os.path.join(full_path, main_file_name), link_path)


setup()